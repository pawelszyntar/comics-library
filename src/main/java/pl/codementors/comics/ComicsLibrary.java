package pl.codementors.comics;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    private static final Logger log = Logger.getLogger(ComicsLibrary.class.getCanonicalName()); // ZADANIE 8

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {
        return Collections.unmodifiableCollection(comics);
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if (!comics.contains(comic) && comic != null) {
            comics.add(comic);
        }
    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
        if (comics.contains(comic)) {
            comics.remove(comic);
        }
    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {
        comics.forEach(comic -> comic.setCover(cover));
    }

    /**
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {
        Set<String> authors = new HashSet<>();
        for (Comic comic : comics) {
            if (!authors.contains(comic.getAuthor())) {
                authors.add(comic.getAuthor());
            }
        }
        return Collections.unmodifiableCollection(authors);
    }

    /**
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {
        List<String> series = new ArrayList<>();
        for (Comic comic : comics) {
            if (!series.contains(comic.getSeries())) {
                series.add(comic.getSeries());
            }
        }
        return Collections.unmodifiableCollection(series);
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     * <p>
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     * <p>
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     * <p>
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {
        if (file.exists() && !file.isDirectory() && file.isFile() && file.canRead()) {
            try (FileReader fr = new FileReader(file)) {
                Scanner scanner = new Scanner(fr);
                if (scanner.hasNextInt()) {
                    int firstLine = scanner.nextInt();
                    scanner.skip("\n");
                    Comic.Cover cover1;
                    for (int i = 0; i < firstLine; i++) {
                        String title = scanner.nextLine();
                        String author = scanner.nextLine();
                        String series = scanner.nextLine();
                        String cover = scanner.next();
                        if (cover.equals("SOFT")) {
                            cover1 = Comic.Cover.SOFT;
                        } else {
                            cover1 = Comic.Cover.HARD;
                        }
                        int publishMonth = scanner.nextInt();
                        int publishYear = scanner.nextInt();
                        scanner.skip("\n");
                        comics.add(new Comic(title, author, series, cover1, publishYear, publishMonth));
                    }
                }
            } catch (IOException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex); // ZADANIE 8
            }
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {
        int seriesCount = 0;
        for (Comic comic : comics) {
            if (comic.getSeries().equals(series)) {
                seriesCount += 1;
            }
        }
        return seriesCount;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {
        int authorCount = 0;
        for (Comic comic : comics) {
            if (comic.getAuthor().equals(author)) {
                authorCount += 1;
            }
        }
        return authorCount;
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        int yearCount = 0;
        for (Comic comic : comics) {
            if (comic.getPublishYear() == year) {
                yearCount += 1;
            }
        }
        return yearCount;
    }

    /**
     * Counts all comics with the same provided publish hear and month.
     *
     * @param year  Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {
        int yearAndMonthCount = 0;
        for (Comic comic : comics) {
            if (comic.getPublishYear() == year && comic.getPublishMonth() == month) {
                yearAndMonthCount += 1;
            }
        }
        return yearAndMonthCount;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year) {
        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            int yearRead = iterator.next().getPublishYear();
            if (yearRead < year) {
                iterator.remove();
            }
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {
        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            String authorRead = iterator.next().getAuthor();
            if (authorRead.equals(author))
                iterator.remove();
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {
        Map<String, Collection<Comic>> comicsMap = new HashMap<>();
        for (Comic comic : comics) {
            String author = comic.getAuthor();
            if (!comicsMap.keySet().contains(author)) {
                comicsMap.put(author, new HashSet<Comic>());
            }
            comicsMap.get(author).add(comic);
        }
        return comicsMap;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {
        Map<Integer, Collection<Comic>> comicsMap = new HashMap<>();
        for (Comic comic : comics) {
            Integer year = comic.getPublishYear();
            if (!comicsMap.keySet().contains(year)) {
                comicsMap.put(year, new HashSet<Comic>());
            }
            comicsMap.get(year).add(comic);
        }
        return comicsMap;
    }

//    ZADANIE 5

    public void removeAllNewerThan(int year) {
        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            int yearRead = iterator.next().getPublishYear();
            if (yearRead > year) {
                iterator.remove();
            }
        }
    }

//    ZADANIE 6

    public void removeAllFromYearAndMonth(int year, int month) {
        Iterator<Comic> iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = iterator.next();
            int yearRead = comic.getPublishYear();
            int monthRead = comic.getPublishMonth();
            if (yearRead == year && monthRead == month) {
                iterator.remove();
            }
        }
    }

//    ZADANIE 7

    public Map<Pair<Integer, Integer>, Collection<Comic>> getYearsMonthsComics() {
        Map<Pair<Integer, Integer>, Collection<Comic>> comicsMapYM = new HashMap<>();
        for (Comic comic : comics) {
            Integer monthRead = comic.getPublishMonth();
            Integer yearRead = comic.getPublishYear();
            Pair<Integer, Integer> newPair = new ImmutablePair<>(monthRead, yearRead);
            if (!comicsMapYM.keySet().contains(newPair)) {
                comicsMapYM.put(newPair, new HashSet<Comic>());
            }
            comicsMapYM.get(newPair).add(comic);
        }
        return comicsMapYM;
    }

}

